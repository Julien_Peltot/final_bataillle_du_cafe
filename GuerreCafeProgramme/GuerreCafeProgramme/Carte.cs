﻿using System;
using System.Collections.Generic;

namespace GuerreCafeProgramme
{
    public static class Carte
    {
        public static List<Parcelle> parcellesCarte = new List<Parcelle>(); 
        public static Case[,] casesCartes = new Case[10, 10];
        public static char[,] graineCartes = new char[10, 10];
        public static int nbParcelles = 0;

        public static void InitialisationCase()
        {
            for (int compteurX = 0; compteurX < 10; compteurX++)
            {
                for (int compteurY = 0; compteurY < 10; compteurY++)
                {
                    casesCartes[compteurX, compteurY] = new Case(0, "Neutre", true, compteurX, compteurY, 0, 0);
                }
            }
        }


        public static void IniatialisationGraine()
        {
            for (int compteurX = 0; compteurX < 10; compteurX++)
            {
                for (int compteurY = 0; compteurY < 10; compteurY++)
                {
                    graineCartes[compteurX, compteurY] = 'R';
                }
            }
        }

        public static void IniatialiationParcelles()
        {
            for (int compteur = 0; compteur < nbParcelles; compteur++)
                parcellesCarte.Add(new Parcelle(compteur, "neutre", 0));
        }

        public static void AffichageCarte()
        {
            Console.WriteLine("Carte :64 = mer, 32 = foret)\nLe chiffre = nombre parcelle\n");
            for (int compteurX = 0; compteurX < 10; compteurX++)
            {
                for (int compteurY = 0; compteurY < 10; compteurY++)
                {
                    if (Carte.casesCartes[compteurX, compteurY].GetNbParcelle() < 10)
                    {
                        Console.Write("  " + Carte.casesCartes[compteurX, compteurY].GetNbParcelle());
                    }
                    else
                    {
                        Console.Write(" " + Carte.casesCartes[compteurX, compteurY].GetNbParcelle());
                    }

                }
                Console.WriteLine("");
            }
        }

        /*
         *  Va permettre d'afficher la carte une fois la parti fini
         */
        public static void AffichageCarteGraines()
        {
            Console.WriteLine("Affichage de la carte finale : ( J = joueur, S = serveur, X = Personne)\n\n");
            for (int CaseEnX = 0; CaseEnX < 10; CaseEnX++)
            {
                for (int CaseEnY = 0; CaseEnY < 10; CaseEnY++)
                {
                    if (Carte.graineCartes[CaseEnX, CaseEnY] == 'J') { Console.ForegroundColor = ConsoleColor.Cyan; }
                    if (Carte.graineCartes[CaseEnX, CaseEnY] == 'S') { Console.ForegroundColor = ConsoleColor.Red; }
                    if (Carte.graineCartes[CaseEnX, CaseEnY] == 'X') { Console.ForegroundColor = ConsoleColor.White; }
                    Console.Write(" " + Carte.graineCartes[CaseEnX, CaseEnY]);
                }
                Console.WriteLine("");
            }
            Console.ForegroundColor = ConsoleColor.Gray;
        }



    }
}
