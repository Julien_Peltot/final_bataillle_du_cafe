﻿using System.Collections.Generic;

namespace GuerreCafeProgramme
{
    public class Parcelle 
    {
        private int NombreParcelle; 
        private string Type;
        private int GrandeurParcelle;
        private List<Case> CaseDeParcelle;
        private int ParcelleValide;
        

        public Parcelle(int nombreParcelle, string type, int grandeurParcelle)
        {
            this.NombreParcelle = nombreParcelle;
            this.Type = type;
            this.GrandeurParcelle = grandeurParcelle;
            this.CaseDeParcelle = new List<Case>();
            this.ParcelleValide = 0;

        }

        public int GetNombreParcelle()
        {
            return this.NombreParcelle;
        }

        public string GetType()
        {
            return this.Type;
        }

        public int GetGrandeurParcelle()
        {
            return this.GrandeurParcelle;
        }

        public Case GetCaseDeParcelle(int numero)
        {
            return CaseDeParcelle[numero];
        }

        public int GetParcelleValide()
        {
            return this.ParcelleValide;
        }

        public void SetNombreParcelle(int nombreParcelle)
        {
            this.NombreParcelle = nombreParcelle;
        }
        public void SetType(string type)
        {
            this.Type = type;
        }
        public void SetGrandeurParcelle(int grandeurParcelle)
        {
            this.GrandeurParcelle = grandeurParcelle;
        }
        public void SetCaseDeParcelle(Case caseDeParcelle)
        {
            this.CaseDeParcelle.Add(caseDeParcelle);
        }
        public void SetParcellesValide(int parcelleValide)
        {
            this.ParcelleValide = parcelleValide;
        }

       
        // on créé les parcelles avec la liaison aux cases de cette derniere et  on les stocks dans la carte
        public static void CaseDeParcelleMisAJour(Case Case, int positionX, int positionY)
        
        {
            if ((Case.GetFrontiereNS() == 1 | Case.GetFrontiereNS() == 2) & (Case.GetFrontiereEO() == 1 | Case.GetFrontiereEO() == 2)) 
            {
                //vide car les cases sont deja a jour
            }
            else
            {
                if (Case.GetFrontiereNS() == -1 | Case.GetFrontiereNS() == 0) 
                {
                    Case.SetNbParcelle(Carte.casesCartes[positionX - 1, positionY].GetNbParcelle()); 
                }
                if (Case.GetFrontiereEO() == -1 | Case.GetFrontiereEO() == 0) //que est ou aucun
                {
                    Case.SetNbParcelle(Carte.casesCartes[positionX, positionY - 1].GetNbParcelle()); 
                }
            }
        }
        public static void ReunionCaseNbParcelle()
        {
 
            for (int compteurX = 0; compteurX < 10; compteurX++)
            {
                for (int compteurY = 0; compteurY < 10; compteurY++)
                {
                    DecodageDonnee.CreationParcelle(Carte.casesCartes[compteurX, compteurY]);
                }
            }


            for (int compteurX = 0; compteurX < 10; compteurX++)
            {
                for (int compteurY = 0; compteurY < 10; compteurY++)
                {
                    Parcelle.CaseDeParcelleMisAJour(Carte.casesCartes[compteurX, compteurY], compteurX, compteurY);
                }
            }

        }

        // Lien entre les parcelle est leurs cases
        public static void LiaisonCasesParcelle()
        
        {
            for (int compteurDeParcelle = 0; compteurDeParcelle < Carte.nbParcelles; compteurDeParcelle++)
            {
                for (int compteurX = 0; compteurX < 10; compteurX++)
                {
                    for (int compteurY = 0; compteurY < 10; compteurY++)
                    {
                        if (Carte.casesCartes[compteurX, compteurY].GetNbParcelle() == Carte.parcellesCarte[compteurDeParcelle].GetNombreParcelle())
                        {
                            Carte.parcellesCarte[compteurDeParcelle].SetCaseDeParcelle(Carte.casesCartes[compteurX, compteurY]); 
                            Carte.parcellesCarte[compteurDeParcelle].SetGrandeurParcelle(Carte.parcellesCarte[compteurDeParcelle].GetGrandeurParcelle() + 1); 
                        }
                    }
                }
                Carte.parcellesCarte[compteurDeParcelle].SetType(Carte.parcellesCarte[compteurDeParcelle].GetCaseDeParcelle(0).Gettype()); 
                if (Carte.parcellesCarte[compteurDeParcelle].GetType() != "neutre")
                {
                    Carte.parcellesCarte[compteurDeParcelle].SetParcellesValide(Carte.parcellesCarte[compteurDeParcelle].GetGrandeurParcelle()); 

                }
            }
        }


        public static void InitialisationParcellesCarte()
 
        {
            //donne aux dernieres cases leur numero de parcelle
            Parcelle.ReunionCaseNbParcelle();
            //initialisation parcelles
            Carte.IniatialiationParcelles();
            //liaison cartes et parcelles
            Parcelle.LiaisonCasesParcelle();
        }

        public static void caseValableParcelle(int grandeurParcelle, ref bool caseTrouve, ref string caseChoisie, int positionX, int positionY, ref string caseChoisieDeuxiemeChoix, ref string caseChoisieTroisiemeChoix, ref string caseChoisieAleatoirement, bool PlanDeSecour = false)
        //On choisie une case le plus pertinament possible pour avoir le maximum de point

        {
            int NombreGraineJoueur = 0;
            int NombreGraineAdversaire = 0;
            int NombreGraineNULL = 0;
            int NumeroParcelleAnalyse = Carte.casesCartes[positionX, positionY].GetNbParcelle();
            foreach (Parcelle parcelle in Carte.parcellesCarte) //on cherche la parcelle liee au numero de parcelle de la case
            {
                if ((parcelle.GetGrandeurParcelle() == grandeurParcelle) & (parcelle.GetNombreParcelle() == NumeroParcelleAnalyse) & (!caseTrouve)) 
                {
                    NombreGraineAdversaire = 0;
                    NombreGraineJoueur = 0;
                    for (int compteurCase = 0; compteurCase < grandeurParcelle; compteurCase++) 
                    {
                        int cooXCase = parcelle.GetCaseDeParcelle(compteurCase).GetPositionX();
                        int cooYCase = parcelle.GetCaseDeParcelle(compteurCase).GetPositionY();
                        if (Carte.graineCartes[cooXCase, cooYCase] == 'O') { NombreGraineJoueur++; }
                        if (Carte.graineCartes[cooXCase, cooYCase] == 'N') { NombreGraineAdversaire++; }
                        if (Carte.graineCartes[cooXCase, cooYCase] == 'R') { NombreGraineNULL++; }
                    }

                    if ((NombreGraineJoueur - NombreGraineAdversaire == 0) & (grandeurParcelle == 3) & (!caseTrouve)) 
                    {
                        caseChoisie += positionX.ToString() + positionY.ToString();
                        caseTrouve = true;

                    }

                    if ((NombreGraineJoueur - NombreGraineAdversaire == 1) & (!caseTrouve)) 
                    {
                        caseChoisie += positionX.ToString() + positionY.ToString();
                        caseTrouve = true;
                    }

                    if ((NombreGraineNULL == grandeurParcelle) & (!caseTrouve)) 
                    {
                        caseChoisie += positionX.ToString() + positionY.ToString();
                        caseTrouve = true;
                    }

                }
            }

            if (PlanDeSecour)
            {
                //si l'on ne trouve rien pour optimiser les points a avoir

                if ((NombreGraineJoueur == NombreGraineAdversaire)) 
                {
                    caseChoisieDeuxiemeChoix = "A:" + positionX.ToString() + positionY.ToString();
                }

                if ((NombreGraineJoueur - NombreGraineAdversaire < 0)) 
                {
                    caseChoisieTroisiemeChoix = "A:" + positionX.ToString() + positionY.ToString();
                }

                if (true) 
                {
                    caseChoisieAleatoirement = "A:" + positionX.ToString() + positionY.ToString();
                }

            }
        }


    }
}
