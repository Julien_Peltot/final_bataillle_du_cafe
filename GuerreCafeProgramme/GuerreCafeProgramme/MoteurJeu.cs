﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets; // pour les sockets

namespace GuerreCafeProgramme
{
    public class MoteurJeu
    {


        //Choix case effectué par le joueur
        public static void ChoixCase(int grandeurParcelleRechercher, string CoordonneeDerniereGrainePoser, ref bool casetrouver, ref string caseChoisie, ref string caseChoisieDeuxiemeChoix, ref string caseChoisieTroisiemeChoix, ref string CaseChoisieAleatoirement, bool PlanSerour = false, bool caseProche = false)
        
        {
            int positionX = (int)Char.GetNumericValue(CoordonneeDerniereGrainePoser[2]);
            int positionY = (int)Char.GetNumericValue(CoordonneeDerniereGrainePoser[3]);
            //Cherche par colonne
            for (int compteurY = 0; compteurY <= 9; compteurY++) 
            {
                if ((Carte.casesCartes[positionX, compteurY].GetPlantable()) & (!casetrouver)) //si la case est plantable
                {
                    if (!casetrouver) { Parcelle.caseValableParcelle(grandeurParcelleRechercher, ref casetrouver, ref caseChoisie, positionX, compteurY, ref caseChoisieDeuxiemeChoix, ref caseChoisieTroisiemeChoix, ref CaseChoisieAleatoirement, PlanSerour); }
                    if ((!casetrouver) & (caseProche)) { ObtentionPtsCaseProche(grandeurParcelleRechercher, ref casetrouver, ref caseChoisie, positionX, compteurY); }
                }
            }

            if (!casetrouver)
            {
                //cherche par ligne
                for (int compteurX = 0; compteurX <= 9; compteurX++)
                {
                    if ((Carte.casesCartes[compteurX, positionY].GetPlantable()) & (!casetrouver)) 
                    {
                        if (!casetrouver) { Parcelle.caseValableParcelle(grandeurParcelleRechercher, ref casetrouver, ref caseChoisie, compteurX, positionY, ref caseChoisieDeuxiemeChoix, ref caseChoisieTroisiemeChoix, ref CaseChoisieAleatoirement, PlanSerour); }
                        if ((!casetrouver) & (caseProche)) { ObtentionPtsCaseProche(grandeurParcelleRechercher, ref casetrouver, ref caseChoisie, compteurX, positionY); }
                    }
                }
            }

        }
        /* Obtention des points en fonction des graines proches
       * grandeurParcelleRechercher: taille des parcelle visées pour obtenir les pts
       * casetrouver : bool pour savoir si une case est trouver
       * CoordCASE : coordonnees de la case qui peut etre trouver
       * CoordonneeX, CoordonneeY : coordonnees de la case actuellement verifiee
       */
        public static void ObtentionPtsCaseProche(int grandeurParcelle, ref bool casetrouver, ref string CoordCASE, int CoordonneeX, int CoordonneeY)

        {
            int enHaut = CoordonneeX - 1;
            int enBas = CoordonneeX + 1;
            int aGauche = CoordonneeY - 1;
            int aDroite = CoordonneeY + 1;
            bool valide = false;
            int numParcelle = Carte.casesCartes[CoordonneeX, CoordonneeY].GetNbParcelle();

            foreach (Parcelle parcelle in Carte.parcellesCarte) //on cherche la parcelle liee au numero de parcelle de la case
            {
                if ((parcelle.GetGrandeurParcelle() == grandeurParcelle) & (parcelle.GetNombreParcelle() == numParcelle) & (!casetrouver)) //si bonne parcelle comme selon le parametre de taille et le num de la case
                {

                    if (enHaut >= 0) //si on depasse pas la limite
                    {
                        if (Carte.graineCartes[enHaut, CoordonneeY] == 'O')
                        {
                            valide = true;
                        }
                    }
                    if (enBas <= 9) //si on depasse pas la limite
                    {
                        if (Carte.graineCartes[enBas, CoordonneeY] == 'O')
                        {
                            valide = true;
                        }
                    }
                    if (aGauche >= 0) //si on depasse pas la limite
                    {
                        if (Carte.graineCartes[CoordonneeX, aGauche] == 'O')
                        {
                            valide = true;
                        }
                    }
                    if (aDroite <= 9) //si on depasse pas la limite
                    {
                        if (Carte.graineCartes[CoordonneeX, aDroite] == 'O')
                        {
                            valide = true;
                        }
                    }

                    if (valide) //si au moins une case caseProche
                    {
                        CoordCASE += CoordonneeX.ToString() + CoordonneeY.ToString();
                        casetrouver = true;
                    }

                }
            }

        }

        //Gere toute la stratégie de l'IA
        public static string MoteurIA(string aEviter, string CoordonneDerniereGraine)
       
        {

            bool caseTrouver = false;
            string caseChoisie = "A:";
            string caseChoisieSecondeZone = "A:";
            string caseChoisieTroisiemeZone = "A:";
            string caseChoisiePasParChoix = "A:";
            //on verifie d'abord selon les points obtenus par parcelles
            ChoixCase(3, CoordonneDerniereGraine, ref caseTrouver, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix);
            if (!caseTrouver) { ChoixCase(6, CoordonneDerniereGraine, ref caseTrouver, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix); }
            if (!caseTrouver) { ChoixCase(4, CoordonneDerniereGraine, ref caseTrouver, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix); }
            if (!caseTrouver) { ChoixCase(2, CoordonneDerniereGraine, ref caseTrouver, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix); }
            //sinon on verifie selon les points obtenus par adjacence
            if (!caseTrouver) { ChoixCase(3, CoordonneDerniereGraine, ref caseTrouver, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix, false, true); }
            if (!caseTrouver) { ChoixCase(6, CoordonneDerniereGraine, ref caseTrouver, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix, false, true); }
            if (!caseTrouver) { ChoixCase(4, CoordonneDerniereGraine, ref caseTrouver, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix, false, true); }
            if (!caseTrouver) { ChoixCase(2, CoordonneDerniereGraine, ref caseTrouver, ref caseChoisie, ref caseChoisieSecondeZone, ref caseChoisieTroisiemeZone, ref caseChoisiePasParChoix, true, true); }
            //sinon, on "fait avec ce qu'on a"
            if (!caseTrouver) { caseChoisie = caseChoisieSecondeZone; }
            if (caseChoisie == "A:") { caseChoisie = caseChoisieTroisiemeZone; }
            if (caseChoisie == "A:") { caseChoisie = caseChoisiePasParChoix; }

            return caseChoisie;
        }

        public static void ChoixJ1(int grandeurParcelle, ref bool caseTrouver, ref string caseChoisie)
 
        {
            int numParcelle = 99;
            for (int compteurX = 0; compteurX <= 9; compteurX++)
            {
                for (int compteurY = 0; compteurY <= 9; compteurY++)
                {
                    numParcelle = Carte.casesCartes[compteurX, compteurY].GetNbParcelle();
                    foreach (Parcelle parcelle in Carte.parcellesCarte) //on cherche la parcelle liee au numero de parcelle de la case
                    {
                        if ((parcelle.GetGrandeurParcelle() == grandeurParcelle) & (parcelle.GetNombreParcelle() == numParcelle) & (!caseTrouver))
                        {
                            caseChoisie += compteurX.ToString() + compteurY.ToString();
                            caseTrouver = true;

                        }
                    }

                }
            }
        }

        public static string Joueur1()
 
        {
            bool caseTrouver = false;
            string caseChoisie = "A:";
            ChoixJ1(3, ref caseTrouver, ref caseChoisie);
            if (!caseTrouver) { ChoixJ1(6, ref caseTrouver, ref caseChoisie); }
            if (!caseTrouver) { ChoixJ1(4, ref caseTrouver, ref caseChoisie); }
            if (!caseTrouver) { ChoixJ1(2, ref caseTrouver, ref caseChoisie); } //il y a au moins un des ces 4 types de parcelles

            return caseChoisie;

        }
        //Permet les communications avec le serveur avec tous les choix modifier
        public static int[] CommunicationAuServeur(Socket laSocket)
        {
           
            bool fin = false;
            string donneesSocket = "";
            string choixPlacement = "";
            string NepasJouer = "";
            string CoordonneeDerniereGraine = "";
            int tour = 1;
            int scoreJ1 = 0, scoreJ2 = 0;

            int positionX = 0;
            int positionY = 0;

            while (!fin)
            {
                //le client joue
                if (tour == 1) { choixPlacement = MoteurJeu.Joueur1(); }
                else { choixPlacement = MoteurJeu.MoteurIA(NepasJouer, CoordonneeDerniereGraine); }
                //Console.WriteLine(choixPlacement); pour deboguer
                JeuSocket.EnvoiDonneeServeur(laSocket, choixPlacement);


                //le client apprend si son coup a marche ou non
                donneesSocket = JeuSocket.RetourStringResultat(laSocket);
                if (donneesSocket == "INVA") { NepasJouer = choixPlacement; }
                if (donneesSocket == "VALI")
                {
                    positionX = (int)Char.GetNumericValue(choixPlacement[2]);
                    positionY = (int)Char.GetNumericValue(choixPlacement[3]);
                    Carte.graineCartes[positionX, positionY] = 'O';
                    Carte.casesCartes[positionX, positionY].SetPlantable(false);
                    Console.WriteLine("Graine plantée en {0},{1} par le client !", positionX + 1, positionY + 1);
                }
                tour++; //tour du joueur


                //le client apprend si l'adversaire a pu jouer ou si la partie est finie
                donneesSocket = JeuSocket.RetourStringResultat(laSocket);
                if (donneesSocket[0] == 'F')
                {
                    fin = true;
                    donneesSocket = JeuSocket.RetourStringResultat(laSocket);
                    JeuSocket.RecupScoreJoueur(donneesSocket, ref scoreJ1, ref scoreJ2);
                }
                if (donneesSocket[0] == 'B')
                {
                    positionX = (int)Char.GetNumericValue(donneesSocket[2]);
                    positionY = (int)Char.GetNumericValue(donneesSocket[3]);
                    Carte.graineCartes[positionX, positionY] = 'N';
                    Carte.casesCartes[positionX, positionY].SetPlantable(false);
                    Console.WriteLine("Graine plantée en {0},{1} par le serveur !", positionX + 1, positionY + 1);
                    CoordonneeDerniereGraine = donneesSocket.Substring(0, 4); //si pas que les donnees de coordonnees
                    tour++; //tour de l'adversaire

                    if (donneesSocket.Length > 4) //au cas ou les deux donnees B:xy et ENCO/FINI sont recuperees ensemble
                    {
                        //le client apprend s'il peut jouer ou si la partie est finie
                        if (donneesSocket[4] == 'E') { /*rien a faire*/ }

                        if (donneesSocket[4] == 'F')
                        {
                            fin = true;
                            donneesSocket = JeuSocket.RetourStringResultat(laSocket);
                            JeuSocket.RecupScoreJoueur(donneesSocket, ref scoreJ1, ref scoreJ2);
                        }
                    }

                    else
                    {
                        //le client apprend s'il peut jouer ou si la partie est finie
                        donneesSocket = JeuSocket.RetourStringResultat(laSocket);
                        if (donneesSocket == "ENCO") { /*rien a faire*/ }
                        if (donneesSocket == "FINI")
                        {
                            fin = true;
                            donneesSocket = JeuSocket.RetourStringResultat(laSocket);
                            JeuSocket.RecupScoreJoueur(donneesSocket, ref scoreJ1, ref scoreJ2);
                        }
                    }


                }


            }

            //on ferme le serveur
            laSocket.Shutdown(SocketShutdown.Both);
            laSocket.Close();
            //on met de l'ordre dans les resultats pour les envoyer
            int[] tab = new int[] { -1, -1, -1 }; //valeurs impossibles, donc efficace pour savoir s'il y a une erreur dans la reception des resultats
            tab[0] = tour; tab[1] = scoreJ1; tab[2] = scoreJ2;




            return tab;
        }
    }
}
