﻿using System;
using System.Net.Sockets; //pour les sockets
using System.Text; //pour le stringbuilder

namespace GuerreCafeProgramme
{
    class GuerreCafeMain
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Appuyer sur une touche pour lancer la Bataille du café");
            Console.ReadKey();

            //initialisation de la Partie avec le serveur
            Console.Write("\nInitialisation de la Socket \nSur quel adresse Ip voulez vous jouez ? ");
            string ip = Console.ReadLine();
            Console.Write("Sur quel port ? : ");
            int port = int.Parse(Console.ReadLine());
            Carte.nbParcelles = 0;
            Socket laSocket = JeuSocket.RecuperationSocket(ip,port);
            Console.WriteLine("Socket initialisée ");
            
            Case.InitValeursCases(laSocket);
            Parcelle.InitialisationParcellesCarte(); //les donnes de la cartes sont initialisees
           
            //Fin initialisation de la partie avec les données du serveur

            Carte.AffichageCarte();


            //PARTI POUR LE SEMESTRE 4 

            Console.WriteLine("\nVeuillez appuyer pour commencer le jeu\n");
            Console.ReadKey();

           //Recreation de partie + initialisation des graines pour le SEMESTRE 4
            Carte.IniatialisationGraine();

            //DEROULEMENT DU JEU
            int[] resultat = MoteurJeu.CommunicationAuServeur(laSocket);

            //affichage des resultats
            StringBuilder final = new StringBuilder();
            Console.WriteLine(final.Append("\nPartie finie en " + resultat[0] + " tours\nLe Joueur a obtenu un score de " + resultat[1] + "\nLe serveur a obtenu un score de " + resultat[2]).ToString());
            Carte.AffichageCarteGraines();

            //fin
            Console.WriteLine("\nAppuyer pour terminer la partie");
            Console.ReadKey();


        }
    }
}
