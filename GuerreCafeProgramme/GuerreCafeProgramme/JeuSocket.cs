﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets; // pour les sockets
namespace GuerreCafeProgramme
{
    public class JeuSocket
    {

        //SEMESTRE 3 : la Socket
        // Initialisation de la socket avec son IP et son Port
        public static Socket RecuperationSocket(string ip, int port)
        {
            Socket LaSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            LaSocket.Connect(ip, port);
            return LaSocket;
        }

        // Le décodage de la socket afin de lui donner une valeur.
        public static void DecodageSocket(string code)

        {
            int compteurX = 0, compteurY, compteurCode = 0;
            StringBuilder codeCase = new StringBuilder("");
            for (int compteur = 0; compteur < 10; compteur++) //pour affecter les 10 lignes de la carte
            {
                compteurY = -1;
                while (compteurX <= compteur) // pour être sur d'avoir lu une ligne en entière
                {
                    codeCase.Clear();
                    compteurY++; // pas 0 car represente l'index de la permiere case du tableau, donc causera un decalage dans les lignes du tableau
                    while ((code.Substring(compteurCode, 1) != ":") & (code.Substring(compteurCode, 1) != "|")) //s'arrête quand il y a un separateur
                    {
                        codeCase.Append(code.Substring(compteurCode, 1));
                        compteurCode++;
                    }
                    DecodageDonnee.DecodageCaseParValeur(codeCase.ToString(), Carte.casesCartes[compteurX, compteurY]);
                    //initialise les parcelles avec au moins les 'murs' nord-ouest
                    if (code.Substring(compteurCode, 1) == "|")
                    {
                        compteurX++;
                    }
                    compteurCode++;
                }
            }
        }

        //Renvoi le retour de la Socket
        public static string RetourStringResultat(Socket LaSocket)
       
        {
            Byte[] bytesReceived = new Byte[256];
            int resultat = 0;
            string code = "";
            do
            {
                resultat = LaSocket.Receive(bytesReceived, bytesReceived.Length, 0);
                code = code + Encoding.ASCII.GetString(bytesReceived, 0, resultat);
            } while (resultat == 256);

            return code;
        }
        

        //SEMESTRE 4 


        //Dialogue avec le serveur
        // Permet  l'utilisateur d'envoyer des données au serveur (laSocket), donnees: les données renvoyé au serveur
        public static void EnvoiDonneeServeur(Socket laSocket, string donnees)
      
        {
            byte[] msg = Encoding.ASCII.GetBytes(donnees);
            laSocket.Send(msg);
        }


        /* Recup le score des Joueurs à partir de la Socket
         * donneesSocket : donnees recues de la socket
         * ScoreJOUEUR1, ScoreJOUEUR2 : score des joueurs
        */
        public static void RecupScoreJoueur(string donneesSocket, ref int ScoreJOUEUR1, ref int ScoreJOUEUR2)
        
        {
            ScoreJOUEUR1 = ((int)Char.GetNumericValue(donneesSocket[2]) * 10) + (int)Char.GetNumericValue(donneesSocket[3]);
            ScoreJOUEUR2 = ((int)Char.GetNumericValue(donneesSocket[5]) * 10) + (int)Char.GetNumericValue(donneesSocket[6]);
        }


    }
}
