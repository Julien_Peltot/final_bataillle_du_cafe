﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuerreCafeProgramme
{
    public class DecodageDonnee
    {
        /**
         * Permet d'assigner à chaque case son numéro de parcelle
         */ 
         public static void CreationParcelle(Case caseChange)
        
        
         {
            if ((caseChange.GetFrontiereNS() == 1 | caseChange.GetFrontiereNS() == 2) & (caseChange.GetFrontiereEO() == 1 | caseChange.GetFrontiereEO() == 2)) //nord ou nord-sud, et ouest ou ouest-est
            {
                if (caseChange.Gettype() == "mer")
                {
                    caseChange.SetNbParcelle(64); //num des parcelles mers
                }
                if (caseChange.Gettype() == "foret")
                {
                    caseChange.SetNbParcelle(32); //num des parcelles forets
                }
                if (caseChange.Gettype() == "neutre")
                {
                    int positionX = caseChange.GetPositionX();
                    int positionY = caseChange.GetPositionY();

                    while ((Carte.casesCartes[positionX, positionY].GetFrontiereNS() == 1 | Carte.casesCartes[positionX, positionY].GetFrontiereNS() == 2) & (Carte.casesCartes[positionX, positionY].GetFrontiereEO() != -1))
                    {
                        positionY++;
                    }
                    if (Carte.casesCartes[positionX, positionY].GetFrontiereNS() == 1 | Carte.casesCartes[positionX, positionY].GetFrontiereNS() == 2)
                    {
                        caseChange.SetNbParcelle(Carte.nbParcelles);
                        Carte.nbParcelles++;

                    }
                    else
                    {
                        while (Carte.casesCartes[positionX, positionY].GetFrontiereNS() != 1)
                        {
                            positionX--;
                        }
                        caseChange.SetNbParcelle(Carte.casesCartes[positionX, positionY].GetNbParcelle());
                    }

                }

            }
        }


        /**
         * DecodageCaseParValeur va permettre de remplacer les valeurs de la frontière et également le Type d'une case
         * Ainsi une case sera typé par Mer, Forêt... 
         * ValeurDecodage : le chiffre à décoder
         * ChangementCase : la case concernée par le changement
         */
        public static void DecodageCaseParValeur(string ValeurDecodage, Case ChangementCase)

         
        {
            int puissance = int.Parse(ValeurDecodage);

            if (puissance - (int)Math.Pow(2, 6) >= 0) //Mer
            {
                ChangementCase.Settype("mer");
                ChangementCase.SetPlantable(false);
                puissance = puissance - (int)Math.Pow(2, 6);
            }

            if (puissance - (int)Math.Pow(2, 5) >= 0) //Foret
            {
                ChangementCase.Settype("foret");
                ChangementCase.SetPlantable(false);
                puissance = puissance - (int)Math.Pow(2, 5);
            }

            if (puissance - (int)Math.Pow(2, 3) >= 0) //Est
            {
                ChangementCase.SetFrontiereEO(-1);
                puissance = puissance - (int)Math.Pow(2, 3);
            }

            if (puissance - (int)Math.Pow(2, 2) >= 0) //Sud
            {
                ChangementCase.SetFrontiereNS(-1);
                puissance = puissance - (int)Math.Pow(2, 2);
            }

            if (puissance - (int)Math.Pow(2, 1) >= 0) //Ouest
            {
                if (ChangementCase.GetFrontiereEO() == -1)
                {
                    ChangementCase.SetFrontiereEO(2);
                }
                else
                {
                    ChangementCase.SetFrontiereEO(1);
                }
                puissance = puissance - (int)Math.Pow(2, 1);
            }

            if (puissance - (int)Math.Pow(2, 0) >= 0) //Nord
            {
                if (ChangementCase.GetFrontiereNS() == -1)
                {
                    ChangementCase.SetFrontiereNS(2);
                }
                else
                {
                    ChangementCase.SetFrontiereNS(1);
                }
                puissance = puissance - (int)Math.Pow(2, 0);
            }
        }

        


    }
}
