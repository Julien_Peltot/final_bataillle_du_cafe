﻿using System.Net.Sockets; // pour les sockets
namespace GuerreCafeProgramme
{
    public class Case
    {
        private int positionX, positionY, frontiereNS, frontiereEO, nbParcelle;
        private string type;
        private bool plantable;


        /**
         * NombreParcelle : le numéro de la parcelle de 0 à 16
         * Type : le Type de la case (Mer, Forêt ou rien)
         * positionX : position dans le tableau par ligne
         * positionY : position dans le tableau par colonne
         * plantable : booleen qui indique si on peut planter ou pas
         * frontereNS : Vérifie la frontiere au nord et au sud afin d'éviter les problèmes de case
         * frontiereEO : pareil que NS mais à l'est et ouest
         */
        public Case(int nbParcelle, string type, bool plantable, int positionX, int positionY, int frontiereEO, int frontiereNS)
        {
            this.nbParcelle = nbParcelle;
            this.type = type;
            this.plantable = plantable;
            this.positionX = positionX;
            this.positionY = positionY;
            this.frontiereEO = frontiereEO;
            this.frontiereNS = frontiereNS;
        }

        public int GetNbParcelle()
        {
            return this.nbParcelle;
        }

        public string Gettype()
        {
            return this.type;
        }

        public bool GetPlantable()
        {
            return this.plantable;
        }

        public int GetPositionX()
        {
            return this.positionX;
        }

        public int GetPositionY()
        {
            return this.positionY;
        }

        public int GetFrontiereNS()
        {
            return this.frontiereNS;
        }

        public int GetFrontiereEO()
        {
            return this.frontiereEO;
        }

        public void SetNbParcelle(int nbParcelle)
        {
            this.nbParcelle = nbParcelle;
        }

        public void Settype(string type)
        {
            this.type = type;
        }

        public void SetPlantable(bool plantable)
        {
            this.plantable = plantable;
        }
        public void SetPositionX(int positionX)
        {
            this.positionX = positionX;
        }
        public void SetPositionY(int positionY)
        {
            this.positionY = positionY;
        }

        public void SetFrontiereNS(int frontiereNS)
        {
            this.frontiereNS = frontiereNS;
        }

        public void SetFrontiereEO(int frontiereEO)
        {
            this.frontiereEO = frontiereEO;
        }
        //Méthodes



        /**
         * Va permettre de recupérer la socket et enfin d'obtenir les valeurs pour initialiser les cases.
         * laSocket : la Socket recuperer sur le serveur
         */
        public static void InitValeursCases(Socket laSocket)
       
        {
            string code = JeuSocket.RetourStringResultat(laSocket);
            Carte.InitialisationCase();
            JeuSocket.DecodageSocket(code);
        }


    }
}
